from django.db import models
from django.conf import settings


class Post(models.Model):
    titulo = models.CharField(max_length=255)
    data_criacao = models.DateTimeField(auto_now_add = True)
    imagem_url = models.URLField(max_length=200, null=True)
    conteudo = models.CharField(max_length=5000)

    def __str__(self):
        return f'{self.titulo} ({self.data_criacao})'